"use strict";

let styleBtn = document.querySelectorAll( '.styleBtn' );

styleBtn.forEach( button => {
    button.addEventListener( 'click', function () {
        let theme = this.dataset.theme;
        applyTheme( theme );
    } );
} );

function applyTheme ( themeName ) {
    document.querySelector( '[title="style"]' ).setAttribute( 'href', `assets/css/style-${ themeName }.css` );
    styleBtn.forEach( button => {
        button.style.display = 'block';
    } );
    document.querySelector( `[data-theme="${ themeName }"]` ).style.display = 'none';
    localStorage.setItem( 'theme', themeName );
}

let activeTheme = localStorage.getItem( 'theme' );

if ( activeTheme === null || activeTheme === 'light' ) {
    applyTheme( 'light' );
} else if ( activeTheme === 'dark' ) {
    applyTheme( 'dark' );
}