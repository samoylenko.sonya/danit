"use strict";

const tabsSelector = document.querySelector( '#tabsSelector' );
const paragraphs = document.querySelectorAll( '.par' );

tabsSelector.addEventListener( 'click', function ( event ) {
    tabsSelector.querySelector( '.active' ).classList.remove( 'active' );
    event.target.classList.add( 'active' );
    const elemId = event.target.id;
    paragraphs.forEach( item => {
        item.hidden = true;
        if ( elemId === item.dataset.filter ) {
            item.hidden = false;
        }
    } )
} )