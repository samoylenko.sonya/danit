"use strict";

let images = new Array();
let i = 0;
let imageTimeout;

images[ 0 ] = './assets/img/1.jpg';
images[ 1 ] = './assets/img/2.jpg';
images[ 2 ] = './assets/img/3.JPG';
images[ 3 ] = './assets/img/4.png';

function viewImages () {
    document.getElementById( "showImage" ).src = images[ i ];
    i++;
    if ( i == images.length ) {
        i = 0;
    }
    imageTimeout = setTimeout( "viewImages()", 3000 );
}

document.querySelector( ".stop" ).addEventListener( "click", () => {
    clearTimeout( imageTimeout );
} )
document.querySelector( ".start" ).addEventListener( "click", () => {
    setTimeout( "viewImages()", 3000 );
} )

viewImages();