"use strict";

const arr = [ "hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv" ];
const showArray = function ( array ) {
    let list = document.createElement( 'ul' );
    const listItems = array.map( item => '<li>' + item + '</li>' );
    for ( let item of listItems ) {
        list.innerHTML += item;
    }
    document.body.append( list );
};
showArray( arr );