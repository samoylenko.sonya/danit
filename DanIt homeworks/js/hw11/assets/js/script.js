"use strict";

const show = document.querySelector( '#show' )
const showRepeat = document.querySelector( '#showRepeat' )
const slash = document.querySelector( '#slash' );
const slashRepeat = document.querySelector( '#slashRepeat' );
slash.style.display = "none";
slashRepeat.style.display = "none";
show.addEventListener( 'click', password );
slash.addEventListener( 'click', password );
showRepeat.addEventListener( 'click', password );
slashRepeat.addEventListener( 'click', password );

function password () {
    const password = document.querySelector( '#password' );
    const repeatPassword = document.querySelector( '#repeatPassword' );
    if ( password.type == "password" && repeatPassword.type == "password" ) {
        password.type = "text";
        repeatPassword.type = "text";
        document.querySelector( '#show' ).style.display = "none";
        document.querySelector( '#showRepeat' ).style.display = "none";
        document.querySelector( '#slash' ).style.display = "inline-block";
        document.querySelector( '#slashRepeat' ).style.display = "inline-block";
    } else {
        password.type = "password";
        repeatPassword.type = "password";
        document.querySelector( '#show' ).style.display = "inline-block";
        document.querySelector( '#showRepeat' ).style.display = "inline-block";
        document.querySelector( '#slash' ).style.display = "none";
        document.querySelector( '#slashRepeat' ).style.display = "none";
    }
}

const button = document.querySelector( '.btn' );
button.addEventListener( 'click', function () {
    if ( document.querySelector( '#password' ).value != document.querySelector( '#repeatPassword' ).value ) {
        alert( "Потрібно ввести однакові значення" );
    } else {
        alert( "You are welcome" );
    }
} );