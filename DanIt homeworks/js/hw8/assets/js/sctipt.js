"use strict";

const paragraphs = document.querySelectorAll( 'p' );
for ( let background of paragraphs ) {
    background.style.backgroundColor = '#ff0000';
}

const findOptionList = document.getElementById( "optionsList" );
console.log( findOptionList );
console.log( findOptionList.parentNode );
console.log( findOptionList.children );

const newParagraph = document.getElementById( 'testParagraph' );
newParagraph.innerHTML = "This is a paragraph";

const mainHeader = document.querySelector( ".main-header" );
const mainHeaderChildNodes = mainHeader.childNodes;
console.log( mainHeaderChildNodes );

const sectionTitle = document.querySelector( ".section-title" );
sectionTitle.classList.remove( "section-title" );
