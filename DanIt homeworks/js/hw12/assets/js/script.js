"use strict";


document.addEventListener( 'keyup', ( event ) => {
    const wrapper = document.querySelector( '.btn-wrapper' );
    const nodes = wrapper.children;
    if ( event.code === 'Enter' ) {
        for ( let n of nodes ) {
            n.style.backgroundColor = 'black';
        }
        nodes[ 0 ].style.backgroundColor = 'blue';
    } else if ( event.code === 'KeyS' ) {
        for ( let n of nodes ) {
            n.style.backgroundColor = 'black';
        }
        nodes[ 1 ].style.backgroundColor = 'blue';
    } else if ( event.code === 'KeyE' ) {
        for ( let n of nodes ) {
            n.style.backgroundColor = 'black';
        }
        nodes[ 2 ].style.backgroundColor = 'blue';
    } else if ( event.code === 'KeyO' ) {
        for ( let n of nodes ) {
            n.style.backgroundColor = 'black';
        }
        nodes[ 3 ].style.backgroundColor = 'blue';
    } else if ( event.code === 'KeyN' ) {
        for ( let n of nodes ) {
            n.style.backgroundColor = 'black';
        }
        nodes[ 4 ].style.backgroundColor = 'blue';
    } else if ( event.code === 'KeyL' ) {
        for ( let n of nodes ) {
            n.style.backgroundColor = 'black';
        }
        nodes[ 5 ].style.backgroundColor = 'blue';
    } else if ( event.code === 'KeyZ' ) {
        for ( let n of nodes ) {
            n.style.backgroundColor = 'black';
        }
        nodes[ 6 ].style.backgroundColor = 'blue';
    }
} );

