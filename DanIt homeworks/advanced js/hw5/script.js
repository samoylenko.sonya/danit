const userUrl = `https://ajax.test-danit.com/api/json/users`;
const postUrl = `https://ajax.test-danit.com/api/json/posts`;
const container = document.querySelector(".container");
const btn = document.querySelector(".btn");
document.body.append(container);

class Card {
  constructor({ name, postName, userName, email, title, body }) {
    this.name = name;
    this.postName = postName;
    this.userName = userName;
    this.email = email;
    this.title = title;
    this.body = body;
  }

  render(container) {
    container.insertAdjacentHTML(
      "beforeend",
      `<div class="card" id ='${this.postName}' >
            <h1>${this.title}</h1>
            <p>${this.body}</p>
            <span>${this.name}</span> </br>
            <span>${this.email}</span>
            <button class="btn">Delete</button>
      </div>`
    );
  }
}

async function getPosts(url) {
  try {
    const posts = await fetch(url).then((response) => response.json());
    return posts;
  } catch (err) {
    console.error(err);
  }
}
function deletePost(id) {
  fetch(`https://ajax.test-danit.com/api/json/posts/${id}`, {
    method: "DELETE",
  }).then((response) => {
    if (response.ok) {
      let block = document.getElementById(`${id}`);
      block.remove();
    } else {
      throw new Error(response.status);
    }
  });
}

getPosts(postUrl).then((res) => {
  res.forEach(({ postName, userId, title, body }) => {
    getPosts(userUrl).then((users) => {
      users.forEach(({ id, name, userName, email }) => {
        if (userId === id) {
          new Card({ name, postName, userName, email, title, body }).render(
            container
          );
        }
      });
    });
  });
});

container.addEventListener("click", (e) => {
  if (e.target.closest("button")) {
    const postWrapper = e.target.closest("div");
    const num = e.target.closest("div").id;
    deletePost(num);
  }
});
