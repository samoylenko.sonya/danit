const container = document.querySelector(".container");
const btn = document.querySelector(".btn");

btn.addEventListener("click", getInfo);

async function getIp(url) {
  const response = await fetch(url);
  if (response.ok) {
    return response.json();
  }
  throw new Error(response.status);
}

async function getAddres() {
  const { ip } = await getIp(`https://api.ipify.org/?format=json`);
  const addres = await getIp(
    `http://ip-api.com/json/${ip}?fields=status,continent,country,region,regionName,city,district`
  );
  return addres;
}

async function getInfo() {
  const { continent, country, region, city, district } = await getAddres();
  container.insertAdjacentHTML(
    "beforeend",
    `<p>Континент: ${continent}</p>
     <p>Країна: ${country}</p>
     <p>Регіон: ${region}</p>
     <p>Місто: ${city}</p>
     <p>Район: ${district}</p>`
  );
}
