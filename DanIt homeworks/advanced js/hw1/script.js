class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  get name() {
    return this._name;
  }

  set name(name) {
    this._name = name;
  }

  get age() {
    return this._age;
  }

  set age(age) {
    this._age = age;
  }

  get salary() {
    return this._salary;
  }

  set salary(salary) {
    this._salary = salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age);
    this.lang = lang;
    this.salary = salary;
  }

  get lang() {
    return this._lang;
  }
  set lang(lang) {
    this._lang = lang;
  }
  get salary() {
    return this._salary;
  }
  set salary(salary) {
    this._salary = salary * 3;
  }
}

const firstProgrammer = new Programmer("Tom", 44, 2000, "English, Ukrainian");
console.log(
  firstProgrammer.name + ",",
  firstProgrammer.age + " years old,",
  firstProgrammer.salary + "$,",
  firstProgrammer.lang + "."
);
const secondProgrammer = new Programmer(
  "Vova",
  30,
  6000,
  "English, Ukrainian, German"
);

console.log(
  secondProgrammer.name + ",",
  secondProgrammer.age + " years old,",
  secondProgrammer.salary + "$,",
  secondProgrammer.lang + "."
);
