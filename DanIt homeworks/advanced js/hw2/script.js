class NoPropertyError extends Error {
  constructor(property) {
    super();
    this.name = "NoPropertyError";
    this.message = `No property ${property} in book #${book}`;
  }
}

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const divArray = document.createElement("div");
divArray.id = "root";
document.body.append(divArray);
const list = document.createElement("ul");
list.id = "list";
divArray.append(list);
let book = 0;

class Books {
  constructor(author, name, price) {
    this.li = document.createElement("li");
    book++;
    if (author == undefined) {
      throw new NoPropertyError("author");
    }
    if (name == undefined) {
      throw new NoPropertyError("name");
    }
    if (price == undefined) {
      throw new NoPropertyError("price");
    }

    this._author = "Author: " + author + ". ";
    this._name = "Name: " + name + ". ";
    this._price = "Price: " + price + ". ";
  }

  createElement() {
    this.li.append(this._author, this._name, this._price);
  }

  render(container = document.querySelector("#list")) {
    this.createElement();
    container.append(this.li);
  }
}

books.forEach((el) => {
  try {
    new Books(el.author, el.name, el.price).render();
  } catch (err) {
    if (err.name === "NoPropertyError") {
      console.log(err);
    } else {
      throw err;
    }
  }
});
