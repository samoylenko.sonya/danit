const gulp = require( 'gulp' );
const browserSync = require( 'browser-sync' ).create();
const sass = require( 'gulp-sass' )( require( 'sass' ) );
const clean = require( 'gulp-clean' );
const concat = require( 'gulp-concat' );
const gp_uglify = require( 'gulp-uglify' );

function clear () {
    return gulp.src( './dist/*', {
        read: false
    } )
        .pipe( clean() );
}

exports.clear = clear;

function buildStyles () {
    return gulp.src( './src/scss/*.scss' )
        .pipe( sass( { outputStyle: 'compressed' } ).on( 'error', sass.logError ) )
        .pipe( concat( 'styles.min.css' ) )
        .pipe( gulp.dest( './dist/css' ) );
};

exports.buildStyles = buildStyles;

function buildScripts () {
    return gulp.src( './src/js/*.js' )
        .pipe( concat( 'scripts.min.js' ) )
        .pipe( gp_uglify() )
        .pipe( gulp.dest( './dist/js' ) )
}


exports.buildScripts = buildScripts;

function watchStyles () {
    gulp.watch( './src/scss/*.scss', buildStyles ).on( 'change', browserSync.reload );
};

exports.watchStyles = watchStyles;

function watchScripts () {
    gulp.watch( './src/js/*.js', buildScripts ).on( 'change', browserSync.reload );
}

exports.watchScripts = watchScripts;

function serve ( cb ) {
    browserSync.init( {
        server: {
            baseDir: "./"
        }
    } );
    cb();
}

exports.serve = serve;

exports.build = gulp.parallel( clear, buildStyles, buildScripts );
exports.dev = gulp.series( serve, gulp.parallel( buildStyles, buildScripts, watchStyles ) );